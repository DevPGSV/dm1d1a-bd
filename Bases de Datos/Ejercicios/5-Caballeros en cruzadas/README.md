# Caballeros en cruzadas

Base de datos para un profe de historia

* Caballeros: nombre y fecha de nacimiento, de quien son hijos (de caballeros), provincia de nacimiento, provincia gobernaron, en que cruzada participó (bajo ordenes de que Rey)
* Provincia: numero de habitantes, nombre, caballeros que la han gobernado en cada fecha (comienzo y fin de gobierno)
* Cruzadas: nombre, contra quien, en una cruzada puede participar más de un Rey
* Rey: nombre, país que reina, fecha de comienzo y fin de reinado.
