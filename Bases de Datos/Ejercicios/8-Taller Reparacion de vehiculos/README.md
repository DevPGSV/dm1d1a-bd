# Taller Reparacion de vehiculos

Taller de repararcion de vehículos

Las averías son notificadas a clientes mediante un parte de averías.
Los datos que se guardan son: descripción de avería, piezas a cambiar e importe de la reparación.
Los clientes del taller se identifican mediante un número de cliente, nombre, y matrícula de vehículo. 

Las nuevas piezas que sustituyen a las averiadas están almacenadas en el taller todo ello gracias al suministro de los proveedores correspondientes. 
De los proveedores queremos guardar el código de proveedor, su teléfono y su nombre.

Las piezas se identifican por un número, y queremos guardar una descripción, su precio, y el stock disponible.

Nos interesa saber para cada proveedor y pieza suministrada el número de unidades y el precio por unidad.

Queremos guardar información sobre las facturas que pueden incluir todos los partes de avería notificados al cliente. Queremos saber el importe total y la fecha de la factura.
