# Aeronáutica

Consorcio de empresas aeronáuticas tienen aeropuertos tiene hangares en alquiler. Cada hangar, una aeronave. Se alquila a un único cliente, aunque un cliente puede alquilar varios.

Los empleados están clasificados como técnicos y comerciales.

Los técnicos se agrupan en equipos. Cada equipo se dedica a una determinada tarea de mantenimiento que puede realizar en varios hangares.

Los comerciales están asociados a la empresa, y se especializan en una determinada zona en la que efectúan visitas a los clientes.

Nos interesa guardar:
empresa: cod, nombre y ciudad
técnicos: cod, nombre y categoría (si dirige equipo o no)
equipo: cod, tarea
hangares: cod, altura, anchura y profundidad de la aeronave que puede alojar
aeronaves: cod,
comerciales: cod, nombre, teléfono
clientes: cod, nombre, teléfono, zona a la que pertenece
zona: cod y nombre
