# Ferretería

Página 49, Ejercicio propuesto 4


Una ferretería vende distintos productos, y cada uno de ellos puede ser creado por varios fabricantes e incluso variar el precio dependiendo de su fabricante.
Cada vez que un cliente compra uno o varios productos se genera una factura que incluye la fecha, los productos comprados, la cantidad de cada producto, el precio unitario y el porcentaje de impuestos sobre el total de la factura.
