# Juego de paises y colonias

Se guarda información de un videojuego.

Se guardan los países, con su nombre y número de habitantes.
En el turno actual, que países están en guerra con cuales otros.
Se guardan los productos, su nombre, cantidad vendida y comprada por cada país.
Se guardan las colonias, a que país pertenecen, su nombre, el número de habitantes, qué productos elaboran y la cantidad de cada producto.
Se guarda el rey de cada país y su color de ojos.
